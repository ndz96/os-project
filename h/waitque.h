#ifndef _waitque_h_
#define _waitque_h_

class PCB;

class SemWaitQueue {
	struct Node {
		Node(PCB* _pcb, int _ticksLeft) :
				pcb(_pcb), ticksLeft(_ticksLeft), next(0) {};
		PCB* pcb;
		int ticksLeft;
		Node* next;
	};
private:
	Node *head, *tail;
public:
	SemWaitQueue() :
			head(0), tail(0) {
	};
	void put(PCB* pcb, int ticksLeft) volatile {
		Node* x = new Node(pcb, ticksLeft);
		if (head == 0)
			head = tail = x;
		else
			tail = tail->next = x;
	}
	PCB* get();
	/*returns number of unblocked threads*/
	int decTicks();
	~SemWaitQueue();
};

#endif
