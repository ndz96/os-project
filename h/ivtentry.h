#ifndef _ivtentry_h_
#define _ivtentry_h_

#define IVT_ENTRY_NUMBER 256

typedef void interrupt (*pInterrupt)(...);

class KernelEv;

class IVTEntry {
private:
	static IVTEntry* entry[IVT_ENTRY_NUMBER];

	pInterrupt oldRoutine;
	KernelEv* myEvent;
	int myEntryNumber;
public:
	static IVTEntry* getEntry(int entryNumber) {
		return entry[entryNumber];
	}

	void setEvent(KernelEv* ev) {
		myEvent = ev;
	}

	IVTEntry(int entryNumber, pInterrupt newRoutine);
	~IVTEntry();

	void callOldRoutine() {
		oldRoutine();
	}

	void signal();

};

#endif
