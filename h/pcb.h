#ifndef _pcb_h_
#define _pcb_h_

#include "thread.h"
#include "dos.h"

#define NUM_SIGNAL 16

extern volatile PCB* running;
extern int blockedGlobally[NUM_SIGNAL];
extern int maskedGlobally[NUM_SIGNAL];

class Semaphore;
class SignalList;

class PCB {
public:
	static enum State{NOT_STARTED, READY, RUNNING, BLOCKED, FINISHED, PAUSED, IDLE};
private:
	static void runwrapper();
	static void finishThread();
	static void signal0Handler();

	static int threadID;
	int id;
	int infiniteTimeSlice;
	int awakenedBySignal;

	unsigned *processStack;

	Semaphore* mySem;
	volatile Time timeSlice;
	volatile Time currTimeSlice;
	Thread *myThread, *parentThread;

	SignalHandler handler[NUM_SIGNAL];
	int maskedHere[NUM_SIGNAL];
	int blockedHere[NUM_SIGNAL];
	SignalList* signalList;
protected:
	friend class Thread;
	friend class SignalList;
public:
	PCB(StackSize stackSize, Time _timeSlice, Thread* _myThread = 0);
	~PCB();
	unsigned sp;
	unsigned ss;
	unsigned bp;
	State state;

	int getID() volatile {
		return id;
	}
	void setTime() volatile {
		currTimeSlice = timeSlice;
	}
	void decTime() volatile {
		currTimeSlice--;
	}
	Time getTime() volatile {
		return currTimeSlice;
	}
	int isInfinite() volatile
	{
		return infiniteTimeSlice;
	}
	void setAwakenedBySignal() volatile
	{
		awakenedBySignal = 1;
	}
	void setAwakenedBySignal0()
	{
		awakenedBySignal = 0;
	}
	int getAwakenedBySignal() const volatile
	{
		return awakenedBySignal;
	}
	Thread* getThread() volatile
	{
		return myThread;
	}

	///signal part
	void signal(SignalId signal);

	void registerHandler(SignalId signal, SignalHandler handle)
	{
		handler[signal] = handle;
	}

	SignalHandler getHandler(SignalId signal) volatile
	{
		return handler[signal];
	}

	void maskSignal(SignalId signal)
	{
		maskedHere[signal] = 1;
	}

	void unmaskSignal(SignalId signal)
	{
		maskedHere[signal] = 0;
	}

	void blockSignal(SignalId signal)
	{
		blockedHere[signal] = 1;
	}

	void unblockSignal(SignalId signal)
	{
		blockedHere[signal] = 0;
	}

	int processSignals() volatile;
	void freeAllocatedResources() volatile;
};

#endif
