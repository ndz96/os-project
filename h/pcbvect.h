#ifndef _pcbvect_h_
#define _pcbvect_h_

#include "pcb.h"

//vector-like class
class PCBVector
{
private:
	int capacity;
	int size;
	PCB** array;
public:
	PCBVector();
	~PCBVector();

	PCB* operator[](int idx)
	{
		return array[idx];
	}

	void push_back(PCB* pcb);

	int getSize() const
	{
		return size;
	}
};

#endif
