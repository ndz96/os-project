#ifndef _usermain_h_
#define _usermain_h_

#include "core.h"
#include "thread.h"

class UserMainTest: public Thread {
private:
	int returnValue;
	int argc;
	char** argv;
public:
	virtual void run();
	UserMainTest(int _argc, char** _argv,
			StackSize stackSize = defaultStackSize, Time timeSlice =
					defaultTimeSlice) :
			Thread(stackSize, timeSlice) {
		argc = _argc;
		argv = _argv;
		returnValue = 0;
	}

	int getReturnValue() const {
		return returnValue;
	}

	~UserMainTest()
	{
		waitToComplete();
		retVal = getReturnValue();
	}
};


#endif
