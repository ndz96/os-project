#ifndef _kernel_h_
#define _kernel_h_

#include <iostream.h>
#include <dos.h>
#include <stdio.h>
#include "SCHEDULE.H"

#define lock asm cli
#define unlock asm sti

#define lockDispatch lockFlag=0;
#define unlockDispatch lockFlag=1;\
	if (context_switch_on_demand){\
		dispatch();\
	}

#define lockDis(cout) lockDispatch\
		cout;\
		unlockDispatch

class PCBVector;
class SemWaitQueue;

extern unsigned oldTimerOFF, oldTimerSEG;
extern unsigned tsp;
extern unsigned tss;
extern unsigned tbp;
extern volatile int context_switch_on_demand;
extern volatile unsigned int lockFlag;
extern PCBVector pcbVect;
extern volatile SemWaitQueue* currentBlockingQueue;

typedef unsigned int Time;
typedef void interrupt (*pInterrupt)(...);
extern Time currentMaxWaitingTime;
extern int retVal;
extern int isIdle;


int userMain(int, char*[]);
void interrupt timer(...);

#endif
