#ifndef _kernsem_h_
#define _kernsem_h_

class SemWaitQueue;
typedef unsigned int Time;

class KernelSem {
public:
	struct Node {
		KernelSem* sem;
		Node* next;
		Node(KernelSem* _sem) {
			sem = _sem;
			next = 0;
		}
	};
private:
	static Node *head, *tail;
	int semValue;
	SemWaitQueue* queue;
protected:
	friend class Semaphore;
	void block(Time maxTimeToWait);
	void deblock();
	void wait(Time maxTimeToWait);
	void signal();
public:
	KernelSem(int init = 1);
	~KernelSem();
	static void decTicksSemaphore();
	static void destroySemList();
};

#endif
