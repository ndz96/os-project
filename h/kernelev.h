#ifndef _kernelev_h_
#define _kernelev_h_

class Semaphore;

typedef unsigned char IVTNo;

class KernelEv{
private:
	Semaphore* sem;
	int creatorThreadId;
	int myEntryNumber;
public:
	KernelEv(IVTNo ivtNo);
	~KernelEv();
	void signal();
	void wait();
};


#endif /* H_KERNELEV_H_ */
