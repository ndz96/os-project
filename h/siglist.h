#ifndef _siglist_h_
#define _siglist_h_

typedef unsigned SignalId;

class PCB;

class SignalList{
	struct Node{
		Node(SignalId _id):id(_id), next(0){};
		SignalId id;
		Node* next;
	};
private:
	Node *head, *tail;
public:
	SignalList():head(0), tail(0){};
	void add(SignalId id);
	int processSignals() volatile;
	~SignalList();
};


#endif
