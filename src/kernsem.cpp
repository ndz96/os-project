#include "kernsem.h"

#include "core.h"
#include "waitque.h"
#include "pcb.h"

KernelSem::Node* KernelSem::head = 0;
KernelSem::Node* KernelSem::tail = 0;

volatile SemWaitQueue* currentBlockingQueue = 0;
Time currentMaxWaitingTime = 0;

KernelSem::KernelSem(int init)
{
	semValue = init;
	queue = new SemWaitQueue();
	Node* x = new Node(this);
	if (head == 0)
		head = tail = x;
	else
		tail = tail->next = x;
}

void KernelSem::decTicksSemaphore()
{
	for (Node *t = head; t != 0; t = t->next)
	{
		if (t->sem != 0)
		{
			int numberUnblockedDueOverWait = t->sem->queue->decTicks();
			t->sem->semValue += numberUnblockedDueOverWait;
		}
	}
}

void KernelSem::block(Time maxTimeToWait)
{
	currentBlockingQueue = queue;
	currentMaxWaitingTime = maxTimeToWait;
	//cout<<"Blokiram nit: "<<Thread::getRunningId()<<endl;
	running->state = PCB::BLOCKED;
	dispatch();
}

void KernelSem::deblock()
{
	PCB* pcb = queue->get();
	pcb->state = PCB::READY;
	//cout<<"Deblokiram nit: "<<pcb->getID()<<endl;
	pcb->setAwakenedBySignal();
	Scheduler::put(pcb);
}

void KernelSem::wait(Time maxTimeToWait)
{
	if (--semValue < 0)
		block(maxTimeToWait);
	else
		running->setAwakenedBySignal();
}

void KernelSem::signal()
{
	if (semValue++ < 0)
		deblock();
}

void KernelSem::destroySemList()
{
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	Node *t = head;
	while (t != 0)
	{
		Node *d = t;
		t = t->next;
		delete d;
	}
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

KernelSem::~KernelSem()
{
	Node *t = head;
	while (1)
	{
		if (t->sem == this)
		{
			t->sem = 0;
			break;
		}
		t = t->next;
	}
	delete queue;
}

