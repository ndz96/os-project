#include "pcb.h"

#include "../h/core.h"
#include "semaphor.h"
#include "pcbvect.h"
#include "siglist.h"

int PCB::threadID = 0;
int blockedGlobally[NUM_SIGNAL] = {0};
int maskedGlobally[NUM_SIGNAL] = {0};

volatile PCB* running;

void idleRun()
{
	while(1){;};
}

void PCB::finishThread()
{
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	running->state = FINISHED;
	int numberOfWaitingThreads = -running->mySem->val();
	for (int i=0; i<numberOfWaitingThreads; ++i)
	{
		running->mySem->signal();
		//immedietly after possible unlock, lock again
	#ifndef BCC_BLOCK_IGNORE
		lock
	#endif
	}
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

void PCB::freeAllocatedResources() volatile
{
	if (processStack != 0)
	{
		delete[] processStack;
		processStack = 0;
	}
	if (signalList != 0)
	{
		delete signalList;
		signalList = 0;
	}
	if (mySem != 0)
	{
		delete mySem;
		mySem = 0;
	}
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
}

void PCB::signal0Handler()
{
	//finish thread violently, wont signal 1 to parent thread
	running->state = FINISHED;
	int numberOfWaitingThreads = -running->mySem->val();
	for (int i=0; i<numberOfWaitingThreads; ++i)
	{
		running->mySem->signal();
	#ifndef BCC_BLOCK_IGNORE
			lock
	#endif
	}

	running->freeAllocatedResources();
}


void PCB::runwrapper()
{
	//finish thread normally, signals 1 to parent thread
	running->myThread->run();
	finishThread();


	if (running->handler[2] != 0 && !running->maskedHere[2] && !maskedGlobally[2])
	{
		lockDis((running->handler[2])();)
	}

	if (running->parentThread != 0)
		running->parentThread->signal(1);

	dispatch();
}


PCB::PCB(StackSize stackSize, Time _timeSlice, Thread* _myThread)
{
	processStack = new unsigned[stackSize];
	processStack[stackSize-1] = 0x200;
	mySem = new Semaphore(0);
#ifndef BCC_BLOCK_IGNORE
	lock
#endif

	sp=ss=bp=0;

	#ifndef BCC_BLOCK_IGNORE
	if(isIdle)
	{
		processStack[stackSize-2] = FP_SEG(idleRun);
		processStack[stackSize-3] = FP_OFF(idleRun);
	}
	else
	{
		processStack[stackSize-2] = FP_SEG(runwrapper);
		processStack[stackSize-3] = FP_OFF(runwrapper);
	}
		sp = FP_OFF(processStack+stackSize-12);
		ss = FP_SEG(processStack+stackSize-12);
		bp = FP_OFF(processStack+stackSize-12);
	#endif

	timeSlice = _timeSlice;
	currTimeSlice = timeSlice;
	myThread = _myThread;

	state = NOT_STARTED;

	if (timeSlice == 0)
		infiniteTimeSlice = 1;
	else
		infiniteTimeSlice = 0;
	awakenedBySignal = 0;
	id = ++threadID;
	pcbVect.push_back(this);

	parentThread = running->getThread();
	for (int i=0; i<NUM_SIGNAL; ++i)
	{
		handler[i] = 0;
		maskedHere[i] = 0;
		blockedHere[i] = 0;
	}

	handler[0] = signal0Handler;
	signalList = new SignalList();
}


PCB::~PCB()
{
	freeAllocatedResources();
}

void PCB::signal(SignalId signal)
{
	signalList->add(signal);
	if (state == PAUSED && !maskedHere[signal] && !maskedGlobally[signal])
	{
		//unpause signaled thread
		state = READY;
		Scheduler::put(this);
	}
}

int PCB::processSignals() volatile
{
	return signalList->processSignals();
}


