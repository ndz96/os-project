#include "event.h"
#include "kernelev.h"
#include "core.h"

Event::Event(IVTNo ivtNo)
{
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	myImpl = new KernelEv(ivtNo);
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

Event::~Event()
{
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	delete myImpl;
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

void Event::wait()
{
	myImpl->wait();
}

void Event::signal()
{
	//empty?
}
