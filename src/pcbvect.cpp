#include "../h/pcbvect.h"

#include "../h/core.h"

PCBVector::PCBVector()
{
	array = new PCB*[1];
	capacity = 1;
	size = 0;
}

void PCBVector::push_back(PCB* pcb)
{
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	array[size++] = pcb;
	if (size == capacity)
	{
		lockDispatch
		PCB** newArray = new PCB*[capacity*2];
		unlockDispatch

		for (int i=0; i<capacity; ++i)
			newArray[i] = array[i];

		lockDispatch
		delete[] array;
		unlockDispatch

		array = newArray;
		capacity *= 2;
	}
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

PCBVector::~PCBVector()
{
	delete[] array;
}

