#include "kernelev.h"
#include "ivtentry.h"
#include "thread.h"
#include "semaphor.h"

KernelEv::KernelEv(IVTNo ivtNo)
{
	IVTEntry* myEntry = IVTEntry::getEntry(ivtNo);
	myEntry->setEvent(this);
	sem = new Semaphore(0);
	creatorThreadId = Thread::getRunningId();
	myEntryNumber = ivtNo;
}

KernelEv::~KernelEv()
{
	IVTEntry* myEntry = IVTEntry::getEntry(myEntryNumber);
	myEntry->setEvent(0);
	delete sem;
}

void KernelEv::wait()
{
	if (Thread::getRunningId() == creatorThreadId)
		sem->wait(0);
}

void KernelEv::signal()
{
	if (sem->val() >= -1)
	{
		sem->signal();
		//dispatch();
	}
}




