#include "ivtentry.h"
#include "core.h"
#include "kernelev.h"

IVTEntry* IVTEntry::entry[] = {0};

IVTEntry::IVTEntry(int entryNumber, pInterrupt newRoutine)
{
	entry[entryNumber] = this;
	oldRoutine = 0;
	myEvent = 0;
	myEntryNumber = entryNumber;

#ifndef BCC_BLOCK_IGNORE
	lock
	oldRoutine = getvect(entryNumber);
	setvect(entryNumber, newRoutine);
	unlock
#endif
}

IVTEntry::~IVTEntry()
{
	entry[myEntryNumber] = 0;
#ifndef BCC_BLOCK_IGNORE
	lock
	setvect(myEntryNumber, oldRoutine);
	unlock
#endif
}

void IVTEntry::signal()
{
	if (myEvent != 0)
		myEvent->signal();
}





