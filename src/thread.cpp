#include "thread.h"

#include "../h/core.h"
#include "../h/pcbvect.h"
#include "pcb.h"
#include "semaphor.h"


Thread::Thread(StackSize stackSize, Time timeSlice)
{
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	myPCB = new PCB(stackSize, timeSlice, this);
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

Thread::~Thread()
{
	waitToComplete();

#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	delete myPCB;
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

void Thread::start()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	myPCB->state = PCB::READY;
	Scheduler::put(myPCB);
	unlock
#endif
}

void Thread::waitToComplete()
{
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	if (myPCB->state != PCB::FINISHED)
	{
		//printf("%d CEKA NA %d\n", getRunningId(), myPCB->getID());
		myPCB->mySem->wait(0);
	}
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

ID Thread::getId()
{
	return myPCB->getID();
}

ID Thread::getRunningId()
{
	return running->getID();
}

Thread* Thread::getThreadById(ID id)
{
	return pcbVect[id-1]->myThread;
}

void dispatch()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	context_switch_on_demand = 1;
	timer();
	unlock
#endif
}


///Signal part
void Thread::signal(SignalId signal)
{
	myPCB->signal(signal);
}

void Thread::registerHandler(SignalId signal, SignalHandler handler)
{
	myPCB->registerHandler(signal, handler);
}

SignalHandler Thread::getHandler(SignalId signal)
{
	return myPCB->getHandler(signal);
}

void Thread::maskSignal(SignalId signal)
{
	myPCB->maskSignal(signal);
}

void Thread::maskSignalGlobally(SignalId signal)
{
	maskedGlobally[signal] = 1;
}

void Thread::unmaskSignal(SignalId signal)
{
	myPCB->unmaskSignal(signal);
}

void Thread::unmaskSignalGlobally(SignalId signal)
{
	maskedGlobally[signal] = 0;
}

void Thread::blockSignal(SignalId signal)
{
	myPCB->blockSignal(signal);
}

void Thread::blockSignalGlobally(SignalId signal)
{
	blockedGlobally[signal] = 1;
}

void Thread::unblockSignal(SignalId signal)
{
	myPCB->unblockSignal(signal);
}

void Thread::unblockSignalGlobally(SignalId signal)
{
	blockedGlobally[signal] = 0;
}

void Thread::pause()
{
	running->state = PCB::PAUSED;
	dispatch();
}





















