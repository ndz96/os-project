#include "waitque.h"

#include "../h/core.h"
#include "SCHEDULE.h"
#include "pcb.h"

PCB* SemWaitQueue::get() {
	if (head == 0) return 0;
	PCB* ret = head->pcb;
	Node* d = head;
	head = head->next;
	delete d;
	if (head == 0)
		tail = 0;
	return ret;
}

int SemWaitQueue::decTicks() {
	int ret = 0;
	Node* prev = 0, *t = head;
	while (t) {
		if (t->ticksLeft == 1) {
			if (prev != 0)
				prev->next = t->next;
			t->pcb->state = PCB::READY;
			t->pcb->setAwakenedBySignal0();

			//cout<<"Deblokiram nit: "<<t->pcb->getID()<<endl;
			Scheduler::put(t->pcb);
			Node* d = t;
			t = t->next;
			if (t == 0)
				tail = prev;
			if (prev == 0)
				head = t;
			delete d;
			ret++;
		} else {
			//ticksLeft = 0 <=> waitUntilSignaled
			if (t->ticksLeft != 0)
				t->ticksLeft--;
			t = t->next;
			if (prev == 0)
				prev = head;
			else
				prev = prev->next;
		}
	}
	return ret;
}

SemWaitQueue::~SemWaitQueue(){
	Node *t = head;
	while (t != 0)
	{
		t->pcb->state = PCB::READY;
		Scheduler::put(t->pcb);

		Node *d = t;
		t = t->next;
		delete d;
	}
}

