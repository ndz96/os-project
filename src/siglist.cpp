#include "siglist.h"
#include "pcb.h"
#include "core.h"

void SignalList::add(SignalId id)
{
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	Node *x = new Node(id);
	if (head == 0)
		head = tail = x;
	else
		tail = tail->next = x;
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
}

int SignalList::processSignals() volatile
{
	Node *prev = 0, *t = head;
	while (t)
	{
		//if masked delete from list and do nothing
		if (maskedGlobally[t->id] || running->maskedHere[t->id])
		{
			if (prev != 0)
				prev->next = t->next;

			Node *d = t;
			t = t->next;

			if (t == 0)
				tail = prev;
			if (prev == 0)
				head = t;
			delete d;
		}
		// if blocked skip until next lookup
		else if (blockedGlobally[t->id] || running->blockedHere[t->id])
		{
			t = t->next;
			if (prev == 0)
				prev = head;
			else
				prev = prev->next;
		}
		// else delete from list and process signal
		else
		{
			if (prev != 0)
				prev->next = t->next;

			//call handler
			SignalHandler handler = running->getHandler(t->id);
			if (handler != 0)
			{
				if (t->id == 0)
				{
					PCB::signal0Handler();

					//return being deleted
					return 0;
				}
				else
				{
					//allow interrupts but not context switch
				#ifndef BCC_BLOCK_IGNORE
					unlock
					lockDis(handler())
					lock
				#endif
				}
			}

			Node *d = t;
			t = t->next;

			if (t == 0)
				tail = prev;
			if (prev == 0)
				head = t;
			delete d;
		}
	}
	return 1;
}


SignalList::~SignalList()
{
	Node *t = head;
	while(t != 0)
	{
		Node *d = t;
		t = t->next;
		delete d;
	}
}




