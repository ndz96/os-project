#include "../h/pcbvect.h"
#include "pcb.h"
#include "thread.h"
#include "waitque.h"
#include "kernsem.h"
#include "semaphor.h"
#include "usermain.h"
#include <stdlib.h>
#include "../h/core.h"
#include "ivtentry.h"


pInterrupt oldTimer;
unsigned tsp;
unsigned tss;
unsigned tbp;


class IdleThread;
volatile int context_switch_on_demand = 0;
volatile unsigned int lockFlag = 1;
volatile PCB* idlePCB;
volatile int isDeadlock = 0;
PCBVector pcbVect;
IdleThread* idleThread;
int isIdle = 0;
Thread* userThread;

int retVal;
//IdleThread* idleThread;


void initialize(int argc, char* argv[]) {
	//initialize main thread
	Thread *tmain = 0;
	running = new PCB(1024, 2, tmain);
	running->state = PCB::RUNNING;

	//initialize userMain thread
	userThread = new UserMainTest(argc, argv, 1024, 2);
	userThread->start();

	//initialize idle thread
	isIdle = 1;
	idlePCB = new PCB(1024, 1);
	idlePCB->state=PCB::IDLE;
	isIdle = 0;
}


void deleteOSThreads()
{
	delete userThread;
	delete running;
	delete idlePCB;
}


void timerInic() {
#ifndef BCC_BLOCK_IGNORE
	lock
	oldTimer = getvect(8);
	setvect(8, timer);
	unlock
#endif
}


void timerRestore() {
#ifndef BCC_BLOCK_IGNORE
	lock
	setvect(8, oldTimer);
	unlock
#endif
}

void tick();

void interrupt timer(...) {
	if (!context_switch_on_demand) {
		KernelSem::decTicksSemaphore();
		tick();
	}

	if (!context_switch_on_demand && !running->isInfinite())
		running->decTime();

	if (context_switch_on_demand
			|| (!running->isInfinite() && running->getTime() == 0)) {

		if (lockFlag) {
			running->setTime();
			context_switch_on_demand = 0;
#ifndef BCC_BLOCK_IGNORE
			asm {
				mov tsp, sp
				mov tss, ss
				mov tbp, bp
			}
#endif

			///SAVE CONTEXT
			running->sp = tsp;
			running->ss = tss;
			running->bp = tbp;

			if (running->state == PCB::BLOCKED)
				currentBlockingQueue->put((PCB*) running, currentMaxWaitingTime);
			else if (running->state != PCB::FINISHED && running->state != PCB::IDLE && running->state != PCB::PAUSED) {
				running->state = PCB::READY;
				Scheduler::put((PCB*) running);
			}


			///RETRIEVE CONTEXT
			running = Scheduler::get();

			while (running != 0 && !running->processSignals())
				running = Scheduler::get();
			if (running == 0)
				running = idlePCB;

			tsp = running->sp;
			tss = running->ss;
			tbp = running->bp;

#ifndef BCC_BLOCK_IGNORE
			asm {
				mov sp, tsp
				mov ss, tss
				mov bp, tbp
			}
#endif
		} else
			context_switch_on_demand = 1;
	}


	if(!context_switch_on_demand)
			oldTimer();
}


int main(int argc, char* argv[]) {
	timerInic();
#ifndef BCC_BLOCK_IGNORE
	lock
#endif
	initialize(argc, argv);
#ifndef BCC_BLOCK_IGNORE
	unlock
#endif
	userThread->waitToComplete();
	timerRestore();
	deleteOSThreads();
	return retVal;
}

